import os
import flask
from flask_cors import CORS
from example_flask_api.settings import Settings
from example_flask_api.views import api
from example_flask_api.version import VERSION
from example_flask_api.models import db
from example_flask_api.schemas import ma

app = flask.Flask(__name__)
app.config["MAX_CONTENT_LENGTH"] = Settings.UPLOAD_SIZE_MAX
app.config['SQLALCHEMY_DATABASE_URI'] = f'mysql://{Settings.username}:{os.environ.get("EXAMPLE_SQL_PASS")}@localhost/{Settings.db_name}'
app.config['SQLALCHEMY_ECHO'] = True
db.init_app(app)
with app.app_context():
    db.create_all()

CORS(app)

@app.route("/")
def root():
    return f"example_flask_api version {VERSION}"

app.register_blueprint(api, url_prefix="/api")

if __name__ == "__main__":
    app.run(port=5000) # Dev server

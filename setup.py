import setuptools
from example_flask_api.version import VERSION

setuptools.setup(
    version=VERSION
)